# Man-Pac
----
Man-Pac è una riproduzione del famoso videogioco [Pac-Man®](https://it.wikipedia.org/wiki/Pac-Man "Pac-Man su Wikipedia.org") sviluppato  
dalla [Namco®](https://it.wikipedia.org/wiki/Namco "Namco Ltd su Wikipedia.org") nel 1980.  
  
Il materiale all'interno di questo repository è rilasciato sotto licenza MIT.  
Vedere il file [`LICENSE.md`](/LICENSE.md) per maggior informazioni.  
Audio e sprite sono proprietà della Namco®.  
  
## Documentazione  
La documentazione è situata nel file `Documentazione.pdf` presente nella  
sezione dowload del repository.  
## Esecuzione  
  
Il gioco è eseguibile mediante il file `Man-Pac.jar` presente nella  
sezione download del repository.