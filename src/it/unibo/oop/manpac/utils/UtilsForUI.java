package it.unibo.oop.manpac.utils;

/**
 * Class containing view utilities.
 */
public final class UtilsForUI {

    /**
     * Default height of the screen.
     */
    public static final int HEIGHT_DEFAULT = 200;
    /**
     * Default width of the screen.
     */
    public static final int WIDTH_DEFAULT = 280;
    /**
     * Default distance of the buttons in the view.
     */
    public static final float DISTANCE_BUTTONS_DEFAULT = 3.0f;
    /**
     * Distance of the button text from the margins.
     */
    public static final float DISTANCE_FROM_MARGINS = 5.0f;
    /**
     * Default size of the button's text.
     */
    public static final float TEXT_SIZE_DEFAULT = 0.4f;
    /**
     * Size of the button's text in the settingsScreen.
     */
    public static final float TEXT_SIZE_SETTINGS = 0.25f;
    /**
     * Default distance from the top in the mainMenuScreen.
     */
    public static final int DEFAULT_DISTANCE_FROM_TOP = 50;
    /**
     * Size of the button's text in the settingsScreen.
     */
    public static final float SETTINGS_TEXT_SIZE = 0.25f;
    /**
     * Default scale for making buttons and fonts smaller.
     */
    public static final float DEFAULT_SCALE = 0.5f;
    /**
     * Size for the phantom's sprite.
     */
    public static final int PHANTOM_SPRITE_SIZE = 10;
    /**
     * Default scale for making gameover screen's labels small enough to fit in the
     * screen.
     */
    public static final float DEFAULT_GAMEOVER_SCREEN_SCALE = 0.4f;
    /**
     * Default scale for making gameover screen's main label bigger than the others.
     */
    public static final float GAMEOVER_SCREEN_MAIN_LABEL_SCALE = 0.8f;
    /**
     * Color of the "YOU WON" label.
     */
    public static final String YOU_WON_COLOR = "3FD23F";
    /**
     * Initial x position of phantoms.
     */
    public static final int PHANTOMS_SPAWN_POSITION_X = 112;
    /**
     * Initial y position of phantoms.
     */
    public static final int PHANTOMS_SPAWN_POSITION_Y = 196;

    /**
     * The radius of a mobile entity.
     */
    public static final int MOBILE_ENTITIES_RADIUS = 5;

    private UtilsForUI() {
    }
}
