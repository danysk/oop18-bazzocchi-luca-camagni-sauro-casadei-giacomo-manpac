package it.unibo.oop.manpac.controller;

/**
 * This class represents the game settings. In a future implementation this will
 * contain the settings to be saved to file, and will have getter/setter for
 * them.
 */
public interface SettingsFile extends java.io.Serializable {

}
