package it.unibo.oop.manpac.model;

/**
 * Represents the directions which an entity can move to.
 *
 */
public enum Directions {

    /**
     * Towards positive y.
     *
     */
    UP, 
    /**
     * Towards negative y.
     *
     */
    DOWN, 
    /**
     * Towards negative x.
     *
     */
    LEFT, 
    /**
     * Towards positive x.
     *
     */
    RIGHT, 
    /**
     * The entity stopped.
     *
     */
    STOP

}
